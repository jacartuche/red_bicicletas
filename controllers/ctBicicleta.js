var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res) {
	Bicicleta.allBicis((err, bicis) => {
		if (err) return console.error(err);
		res.render('bicicletas/index', { 
			title: 'Listado de Bicicletas', 
			bicis: bicis 
		});
	});
}

exports.bicicleta_create_get = function(req, res) {
	res.render('bicicletas/create', { title: 'Crear Bicicletas' });
}

exports.bicicleta_create_post = function(req, res) {
	var bici = new Bicicleta({
		code: req.body.code, 
		color: req.body.color, 
		modelo: req.body.modelo, 
		ubicacion: [req.body.lat, req.body.lng]
	});
	Bicicleta.add(bici, (err, nBici) => {
		if (err) return console.error(err);
		res.redirect('/bicicletas');
	});
}

exports.bicicleta_delete_get = function(req, res) {
	Bicicleta.removeByCode(req.params.code, (err) => {
		if (err) return console.error(err);
		res.redirect('/bicicletas');
	});
}

exports.bicicleta_update_get = function(req, res) {
	Bicicleta.findByCode(req.params.code, (err, bici) => {
		if (err) return console.error(err)
		if (!bici) {
			res.status(404).send("No se encontro Bicicleta");
			return;
		}
		res.render('bicicletas/update', { title: 'Actualizar Bicicletas', bici});
	});
}

exports.bicicleta_update_post = function(req, res) {
	Bicicleta.findByCode(req.params.code, (err, bici) => {
		if (err) return console.error(err)
		if (!bici) {
			res.status(404).send("Error in update: Not found Bicicleta");
			return;
		}
		bici.code = req.body.code;
		bici.color = req.body.color;
		bici.modelo = req.body.modelo;
		bici.ubicacion = [req.body.lat, req.body.lng]
		Bicicleta.updateByCode(req.params.code, bici, (err, opResult) => {
			if (err) return console.error(err)
			res.redirect('/bicicletas');
		});
	});
}

exports.bicicleta_watch_get = function(req, res) {
	Bicicleta.findByCode(req.params.code, (err, bici) => {
		if (err) return console.error(err)
		if (!bici) {
			res.status(404).send("No se encontro Bicicleta");
			return;
		}
		res.render('bicicletas/watch', { title: 'Ver Bicicletas', bici});
	});
}