var Usuario = require("../models/usuario");

exports.usuario_list = function(req, res) {
    Usuario.find({}, (err, usuarios) => {
        res.render('usuarios/index', { usuarios: usuarios });
    });
}

exports.usuario_create_get = function(req, res) {
    res.render('usuarios/create', { errors: {}, usuario: new Usuario() } );
}

exports.usuario_create_post = function(req, res) {
    if (req.body.password != req.body.confirm_password) {
        var objUser = new Usuario({nombre: req.body.nombre, email: req.body.email});
        var json_errors = {
            confirm_password: {
                message: 'No coinciden las contraseñas'
            }
        };
        res.render('usuarios/create', { errors: json_errors, usuario: objUser });
        return;
    }

    var user = {nombre: req.body.nombre, email: req.body.email, password: req.body.password};

    Usuario.create(user, (err, new_usuario) => {
        if (err) {
            var objUser = new Usuario({nombre: req.body.nombre, email: req.body.email});
            res.render('usuarios/create', { errors: err.errors, usuario: objUser });
        } else {
            new_usuario.enviar_mail_bienvenida();
            res.redirect('/usuarios');
        }
    });
}

exports.usuario_update_get = function(req, res) {
    Usuario.findById(req.params.id, (err, usuario) => {
        res.render('usuarios/update', { errors: {}, usuario: usuario } );
    });
}

exports.usuario_update_post = function(req, res) {
    Usuario.findById(req.params.id, (err, targetUser) => {
        if (err) {
            console.log(err);
            res.status(500).send();
            return;
        }

        if (!targetUser) {
            res.status(400).send('No se encontro usuario');
            return;
        }

        if (targetUser && !targetUser.validPassword(req.body.actual_password)) {
            var objUser = new Usuario({_id: req.params.id, nombre: req.body.nombre, email: req.body.email});
            var json_errors = {
                actual_password: {
                    message: 'La contraseña no es correcta'
                }
            };
            res.render('usuarios/update', { errors: json_errors, usuario: objUser });
            return;
        }

        if (req.body.new_password != req.body.confirm_new_password) {
            var objUser = new Usuario({_id: req.params.id, nombre: req.body.nombre, email: req.body.email});
            var json_errors = {
                confirm_new_password: {
                    message: 'No coinciden las contraseñas'
                }
            };
            res.render('usuarios/update', { errors: json_errors, usuario: objUser });
            return;
        }

        // var update_values = {nombre: req.body.nombre, email: req.body.email};
        targetUser.nombre = req.body.nombre;
        targetUser.email = req.body.email;
        if (req.body.new_password && req.body.new_password != "") {
            targetUser.password = req.body.new_password
        }

        targetUser.save((err, usuario) => {
            if (err) {
                console.log(err);
                var objUser = new Usuario({_id: req.params.id, nombre: req.body.nombre, email: req.body.email});
                res.render('usuarios/update', {errors:err.errors, usuario: objUser});
            } else {
                res.redirect('/usuarios');
            }
        });

        // Usuario.findByIdAndUpdate(req.params.id, update_values, (err, usuario) => {
        //     if (err) {
        //         console.log(err);
        //         var objUser = new Usuario({_id: req.params.id, nombre: req.body.nombre, email: req.body.email});
        //         res.render('usuarios/update', {errors:err.errors, usuario: objUser});
        //     } else {
        //         res.redirect('/usuarios');
        //     }
        // });
    });
}

exports.usuario_delete_post = function(req, res, next) {
    Usuario.findByIdAndDelete(req.params.id, function(err) {
        if (err) {
            next(err);
        }
        else {
            res.redirect('/usuarios');
        }
    });
}
