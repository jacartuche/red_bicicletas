const passport = require('../config/passport');
const Usuario = require("../models/usuario");
const Token = require("../models/token");

exports.auth_login_get = function(req, res) {
	res.render('session/login');
}

exports.auth_login_post = function(req, res, next) {
	passport.authenticate('local', function(err, usuario, info) {
		if (err) {
			return next(err);
		}
		if (!usuario) {
			return res.render('session/login', {info});
		}
		req.logIn(usuario, function(err) {
			if (err) {
				return next(err);
			}
			return res.redirect('/');
		});
	})(req, res, next);
}

exports.auth_logout = function(req, res) {
	req.logOut();
	res.redirect('/');
}

exports.auth_forgot_password_get = function(req, res) {
	res.render('session/forgotPassword');
}

exports.auth_forgot_password_post = function(req, res, next) {
	Usuario.findOne({email: req.body.email}, function(err, user) {
		if (!user) {
			return res.render('session/forgotPassword', {
				info: {
					message: 'No existe el email para un usuario existente.'
				}
			});
		}

		user.resetPassword(function(err) {
			if(err) {
				return next(err);
			}
			console.log('session/forgotPasswordMessage');
		});

		res.render('session/forgotPasswordMessage');
	});
}

exports.auth_reset_password_get = function(req, res) {
	Token.findOne({token: req.params.token}, function(err, token) {
		if(!token) {
			return res.status(400).send({
				type: 'not-verified', 
				msg: 'No existe un token asociado. Verifique que su token no haya expirado.'
			});
		}

		Usuario.findById(token._userId, function(err, usuario) {
			if(!usuario) {
				return res.status(400).send({
					msg: 'No existe el usuario asociado al token.'
				});
			}
			res.render('session/resetPassword', {errors: {}, usuario: usuario});
		});
	});
}

exports.auth_reset_password_post = function(req, res) {
	if(req.body.password != req.body.confirm_password) {
		res.render('session/resetPassword', {
			errors: {
				confirm_password: {
					message: 'no coincide las contraseñas'
				}
			},
			usuario: new Usuario({email: req.body.email})
		});
		return;
	}

	Usuario.findOne({email: req.body.email}, function(err, user) {
		user.password = req.body.password;
		user.save(function(err) {
			if(err) {
				res.render('session/resetPassword', {
					errors: err.errors, 
					usuario: new Usuario({email: req.body.email})
				});
			} else {
				res.redirect('/auth/login');
			}
		});
	});
}

// Controller OAuth Google

exports.auth_google = function(req, res, next) {
	passport.authenticate('google', { 
		scope: [
			'profile',
			'email'
		] 
	})(req, res, next);
}

exports.auth_google_callback = function(req, res, next) {
	passport.authenticate('google', {
		successRedirect: '/',
		failureRedirect: '/auth/login' 
	})(req, res, next);
}

// Controller OAuth Facebook

exports.auth_facebook = function(req, res, next) {
	passport.authenticate('facebook')(req, res, next);
}

exports.auth_facebook_callback = function(req, res, next) {
	passport.authenticate('facebook', {
		successRedirect: '/',
		failureRedirect: '/auth/login' 
	})(req, res, next);
}