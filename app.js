require('dotenv').config();
require('newrelic');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var authRouter = require('./routes/auth');
var authAPIRouter = require('./routes/api/auth');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');

let store;

if( process.env.NODE_ENV == 'development' ) {
	store = new session.MemoryStore;
} 
else {
	store = new MongoDBStore({
		uri: process.env.MONGO_URI,
		collection: 'sessions'
	});

	store.on('error', function(error) {
		assert.ifError(error);
		assert.ok(false);
	});
}

var app = express();
app.set('secretKey', 'jwt_pwd_!!223344');
app.use(session({
	cookie: { maxAge: 240 * 60 * 60 * 1000 },
	store: store,
	saveUninitialized: true,
	resave: 'true',
	secret: 'red_bicis_!!!****!!!---!!!"-!!"!.".!".!_!_!_!!!12123'
}));
// Import the mongoose module
var mongoose = require('mongoose');

//Set up default mongoose connection
//mongodb://localhost:27017/red_bicicletas
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.set('useCreateIndex', true);
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
// Get the default connection
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', function() {
  // we're connected!
  console.info("MongoDB connection successfull");
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/auth', authRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/auth', authAPIRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
	if(req.user) {
		next();
	} else {
		console.log('Usuario sin logearse');
		res.redirect('/auth/login');
	}
}

function validarUsuario(req, res, next) {
	jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
		if(err) {
			res.status(401).json({
				status: "error", 
				message: err.message, 
				data: null
			});
		} else {
			req.body.userId = decoded.id;
			console.log('jwt verify: ' + decoded);
			next();
		}
	});
}

module.exports = app;
