# Proyecto Curso NodeJS, Express y MongoDB
Red bicicletas

## Clonar Git
	git clone https://jacartuche@bitbucket.org/jacartuche/red_bicicletas.git

## Iniciar servidor normal
	npm run start

## Iniciar servidor autoreiniciable por cambios
    npm run devstart

## Tests
    num run test

## Versiones
+ [**v1.0-sem-1**](https://bitbucket.org/jacartuche/red_bicicletas/src/v1.0-sem-1/) - Semana 1: Conceptos básicos del desarrollo web del lado servidor. -- [Descargar](https://bitbucket.org/jacartuche/red_bicicletas/get/v1.0-sem-1.zip)
+ [**v2.0-sem-2**](https://bitbucket.org/jacartuche/red_bicicletas/src/v2.0-sem-2/) - Semana 2: Persistencia del modelo utilizando Mongoose y MongoDB. -- [Descargar](https://bitbucket.org/jacartuche/red_bicicletas/get/v2.0-sem-2.zip)
+ [**v3.0-sem-3**](https://bitbucket.org/jacartuche/red_bicicletas/src/v3.0-sem-3/) - Semana 3: Autenticación. -- [Descargar](https://bitbucket.org/jacartuche/red_bicicletas/get/v3.0-sem-3.zip)
+ [**v4.0-sem-4**](https://bitbucket.org/jacartuche/red_bicicletas/src/v4-0-sem-4/) - Semana 4: Oauth y Puesta en producción en Heroku. -- [Descargar](https://bitbucket.org/jacartuche/red_bicicletas/get/v4-0-sem-4.zip)

## Proyecto en producción
[https://red-bicicletas-jacartuche.herokuapp.com/](https://red-bicicletas-jacartuche.herokuapp.com/)

## Variables de ambiente
Este proyecto usa las siguientes variables de ambiente en el entorno local. Deben ir en un archivo .env

	//Entorno de desarrollo
	NODE_ENV = 'development'

	// Link de Conexión a MongoDB
	MONGO_URI = 'mongodb://localhost:27017/red_bicicletas'

	// Envio de emails usando passport con ethereal
	ETHEREAL_USER = 'mohamed.hegmann95@ethereal.email'
	ETHEREAL_PWD = 'Rs5ECjH3cWjmHhEBuW'
	EMAIL_SUPPORT = 'no-reply@redbicicletas.com'

	// URL del servidor (Remplazen el puerto que tiene corriendo nodejs)
	URL_HOSTNAME = 'http://localhost:3000/'

	// Si quieren usar autenticacion por google
	GOOGLE_CLIENT_ID = '<-El id de cliente de OAuth 2.0 de Google Developers->'
	GOOGLE_CLIENT_SECRET = '<-Aqui va la clave secreta del ID->'

	// Si quieren usar autenticacion por facebook token
	FACEBOOK_ID = '<-Identificador de la app obtenida de Facebook Developers->'
	FACEBOOK_SECRET = '<-Clave secreta de la app->'

	// Monitoreo del servidor
	NEW_RELIC_LICENSE_KEY = '<-Su clave de new relic->'

Esta es una captura de las variables de ambiente puestas en producción

![Captura variables en produccion](https://i.ibb.co/0jr0xSM/captura-variables-env-produccion.png)

## Mongo DB Atlas
Para ver que la aplicación productiva usa Mongo Atlas, copien esta cadena de conexión (Connection String) y peguenlo en MongoDB Compass. El usuario tiene permisos solo de lectura.

	mongodb+srv://test_user:Jp3R8ytRBfJLsIoC@cluster-red-bicicletas.kwgm4.mongodb.net/red_bicicletas?retryWrites=true&w=majority

## Rutas
	GET     /
			Página principal

### Auth

    GET     /auth/google
            Inicia el proceso de autenticaciÃ³n por google a travÃ©s de passport
    
    GET     /auth/callback
            Recibe la respuesta y crea o inicia sesiÃ³n a travÃ©s de passport

    POST    /auth/facebook_token
            Recibe "token_access" e inicia sesiÃ³n o crea una cuenta a travÃ©s de passport

    GET     /auth/login
            Inicio de sesión 

    POST    /auth/login
            Procesa los datos del formulario de ingreso y crea la sesión   

    GET     /auth/logout
            Cerrar sesión de usuario    

    GET     /auth/forgotPassword
            Solicitar un correo electrónico para reseteo de contraseña   

    POST    /auth/forgotPassword
            Procesar la solicitud de reseteo de contraseña con los datos enviados

    GET 	/auth/resetPassword/:token
    		Verifica que el token enviado a su correo sea correcto, para redigirlo a cambio de contraseña
    
    POST    /auth/resetPassword
            Procesa los datos del formulario de cambio de contraseña
                
### Token
    GET     /token/confirmation/:token
            Verifica que el token sea correcto para verificar al usuario

### Usuarios
    GET     /usuarios
            Lista de usuarios

    GET     /usuarios/create
            Formulario de creación de usuario
    
    POST    /usuarios/create
            Procesa los datos recibidos por post y crea el usuario

    GET     /usuarios/update/:id
            Formulario de actualización de usuario especificando el id
    
    POST    /usuarios/update/:id
            Procesa los datos recibidos por post y modifica el usuario con id especificado
    
    POST    /usuarios/delete/:id
            Elimina el usuario con id

### Bicicletas
    GET     /bicicletas
            Lista de bicicletas

    GET     /bicicletas/create
            Formulario de creación de bicicletas

    POST    /bicicletas/create
            Recibe los datos del formulario de creación y crea la bicicleta.
    
    GET     /bicicletas/update/:code
            Formulario de modificación de bicicleta

    POST    /bicicletas/update/:code
            Procesa los datos recibidos y actualiza a usuario por code

    POST    /bicicletas/delete/:code
            Borra la bicicleta por su code

    GET     /bicicletas/watch/:code
            Visualiza la bicicleta

            
## API
Para las solicitudes POST, se necesita especificar en el header  
`Content-Type: application/json`

Se recomienda usar Postman.

### Autenticación
	POST    /api/auth/authenticate
            Login de usuario por jwt

            email : correo del usuario
            password : contraseña del usuario

            Devuelve la información del usuario, más un token de acceso para otras apis

    POST    /api/auth/forgotPassword
            Envia correo de recuperación de contraseña

            email : correo del usuario

    POST    /api/auth/facebook_token
            Recupera el usuario dado el token de acceso por facebook

            access_token: token de acceso de facebook

Para probar facebook token, puede usar las siguientes credenciales, o puede generar sus propios usuarios de prueba en Facebook Developers:

Usuario: Ullrich Alegejdgjcfhb Narayananescu  
Email: jganrdjkzf_1605248551@tfbnw.net

	EAAK7L7L1AZCABALJFJd2TjifeZCyWeaob2tKwGuAKAap5lkf1RBMlAs9o0ZCN4JHQ9sIKZCXMvyksZBDxtSJ5nBFkux12RRxlO0w7i69tlpvYOwEiPITmmPGXABOctgMkX3q2ln3qQj46FS8pNHiUZAGAdy6emTj9ZCEYH2rFiLZBkb4RSEtIqfZAykCuNNFvwsXx8GXnIWUe0gZDZD

Usuario: Karen Alegccdigacai Sharpeson  
Email: dupagcrkxa_1605248531@tfbnw.net

	EAAK7L7L1AZCABAAo38pMBzQHZBZBxMk7oZC21CReyQue3SbkmdJekQOAW4lRlV76ZALeZBB7ZCXt0x6qngxNLA9aUtCIKD9PUZC2ZBZBL6a9nEaCiJrQWT4dgppnYC8qZAlADeAuV7wZCZCEqY4QW9R41xmpLdupmjYRpUPROd5MVJPMxQZBvpdUC8J7unun3VuzbIMfKAGGqAuXhgDAZDZD

### Bicicletas
Para usar la api de bicicleta, se necesita logerse por la api de autenticación (jwt).
Se necesita especificar el token de acceso en el header:  
`x-access-token: <-tu token de acceso->`

    GET     /api/bicicletas
    		Devuelve una lista de bicicletas

    GET     /api/bicicletas/:id
    		Devuelve el objeto bicicleta por id (code)

    POST    /api/bicicletas/create
            Se crea el objeto bicicleta

            Se debe especificar en el body:
            code : codigo de bicicleta
            color  : color de la bicicleta
            modelo : modelo de la bicicleta
            lng : longitud de la coordenada
            lat : latitud de la coordenada

    POST    /api/bicicletas/update/:id
            Actualiza el objeto bicicleta

            Se debe especificar en el body:
            code : nuevo codigo de bicicleta
            color  : nuevo color de la bicicleta
            modelo : nuevo modelo de la bicicleta
            lng : nueva longitud de la coordenada
            lat : nueva latitud de la coordenada

    DELETE  /api/bicicletas/delete
            Elimina el objeto bicicleta

            Se debe especificar en el body:
            code : codigo de la bicicleta a eliminar

### Usuarios

	GET     /api/usuarios
    		Devuelve una lista de usuarios

    GET     /api/usuarios/:id
    		Devuelve el objeto usuario por id

    POST    /api/usuarios/create
            Se crea el objeto usuario

            Se debe especificar en el body:
            nombre : nombre del usuario
            email  : correo del usuario
            password : contraseña del usuario

    POST    /api/usuarios/reserva
            Reserva una bicicleta a un usuario

            Se debe especificar en el body:
            id : id del usuario
            bici_id  : id de la bicicleta
            desde : fecha inicio de reserva (GMT)
            hasta : fecha final de reserva (GMT)
