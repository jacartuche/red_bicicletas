const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');
const Usuario = require('../models/usuario');

passport.use(new LocalStrategy(
	function(email, password, done) {
		Usuario.findOne({email: email}, function(err, usuario) {
			if(err) {
				return done(err);
			}
			if(!usuario) {
				return done(null, false, {message: 'Email no existente o incorrecto'});
			}
			if(!usuario.validPassword(password)) {
				return done(null, false, {message: 'Contraseña incorrecta'});
			}
			return done(null, usuario);
		});
	}
));

passport.use(new GoogleStrategy({
		clientID: process.env.GOOGLE_CLIENT_ID,
		clientSecret: process.env.GOOGLE_CLIENT_SECRET,
		callbackURL: process.env.URL_HOSTNAME + 'auth/google/callback'
	},
    
	function(accessToken, refreshToken, profile, done) {
		Usuario.findOneOrCreateByGoogle(profile, function(err, user) {
			return done(err, user);
		});
	}
));

passport.use(new FacebookTokenStrategy({
		clientID: process.env.FACEBOOK_ID,
		clientSecret: process.env.FACEBOOK_SECRET
	}, 
	function(accessToken, refreshToken, profile, done) {
		try{
			Usuario.findOneOrCreateByFacebook(profile, function(err, user) {
				return done(err, user);
			});
		} catch(err2) {
			console.log("error");
			return done(err2, null);
		}
	}
));

passport.use(new FacebookStrategy({
		clientID: process.env.FACEBOOK_ID,
		clientSecret: process.env.FACEBOOK_SECRET,
		callbackURL: process.env.URL_HOSTNAME + "auth/facebook/callback",
		profileFields: ['id', 'displayName', 'photos', 'email']
	},
	function(accessToken, refreshToken, profile, done) {
		Usuario.findOneOrCreateByFacebook(profile, function (err, user) {
			return done(err, user);
		});
	}
));

passport.serializeUser(function(user, callback) {
	callback(null, user.id);
});

passport.deserializeUser(function(id, callback) {
	Usuario.findById(id, function(err, usuario) {
		callback(err, usuario);
	});
});

module.exports = passport;