var mongoose = require('mongoose');
var Reserva = require('./reserva');
const bcrypt = require('bcrypt');
const UniqueValidatorPlugin = require('mongoose-unique-validator');
var Token = require('./token');
var mailer = require('../mailer/mailer');
const crypto = require('crypto');
var Schema = mongoose.Schema;

const saltRounds = 10;

const validateEmail = function(email) {
	const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;
	return re.test(email);
}

var usuarioSchema = new Schema({
	nombre: {
		type: String,
		trim: true,
		required: [true, "El nombre es obligatorio"]
	},
	email:{
		type: String,
		trim: true,
		required: true,
		lowercase: true,
		unique: true,
		validate: [validateEmail, 'Por favor, ingrese un email valido'],
		match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/]
	},
	password: {
		type: String,
		required: [true, 'El password es obligatorio']
    },
	passwordResetToken: String,
	passwordResetTokenExpires: Date,
	googleId: String,
	facebookId: String,
    verificado: {
		type: Boolean,
		default: false
	}
});

usuarioSchema.plugin(UniqueValidatorPlugin, { 
	message: 'El {PATH} ya existe con otro usuario.'
});

usuarioSchema.pre('save', function(next) {
	// Si la contraseña se modificó
	if( this.isModified('password') ) {
		this.password = bcrypt.hashSync(this.password, saltRounds);
	}
	next();
});

usuarioSchema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.resetPassword = function(cb) {
	const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex')});
	const email_destination = this.email;

	token.save(function(err) {
		if (err) { 
			return cb(err); 
		}

		var enlace = process.env.URL_HOSTNAME + 'auth/resetPassword/' + token.token;

		const mailOptions = {
			from: process.env.EMAIL_SUPPORT,
			to: email_destination,
			subject: 'Reseteo de password de cuenta',
			html: 'Hola,<br><br> Por favor, para resetear el password de su cuenta haga click en este enlace:<br>' + 
					'<a href="' + enlace + '" target="_blank">' + enlace + '</a>'
		};

		mailer.sendMail(mailOptions, function(err) {
			if(err) { 
				return cb(err); 
			}
			console.log('Se ha enviado un correo para resetear el password a: ' + email_destination + '.');
		});
	});
}

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
	var reserva = new Reserva({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta, cb: cb });
	reserva.save(cb);
}

usuarioSchema.methods.enviar_mail_bienvenida = function(cb) {
	const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex')});
	const email_destination = this.email;

	token.save(function(err) {
		if(err) { 
			return console.log(err.message); 
		}

		var enlace = process.env.URL_HOSTNAME + 'token/confirmation/' + token.token;

		const mailOptions = {
			from: process.env.EMAIL_SUPPORT,
			to: email_destination,
			subject: 'Verificación de cuenta',
			html: 'Hola,<br><br> Por favor, para verificar su cuenta haga click en este enlace:<br>' + 
					'<a href="' + enlace + '" target="_blank">' + enlace + '</a>'
		};

		mailer.sendMail(mailOptions, function(err) {
			if(err) { 
				return console.log(err.message); 
			}
			console.log('Se ha enviado un correo electrónico de verificación a ' + email_destination + '.');
		});
	});
}

usuarioSchema.statics.findOneOrCreateByGoogle = function(profile, callback) {
	const self = this;
	console.log(profile);

	self.findOne({
		$or:[
			{ 'googleId' : profile.id }, { 'email': profile.emails[0].value }
		]
	}, (err, result) => {
		if(result) {
			callback(err, result);
		} 
		else {
			console.log("--------------CONDITION------------");
			console.log(profile);

			let values = {};

			values.googleId = profile.id;
			values.email = profile.emails[0].value;

			values.nombre = profile.displayName || 'SIN NOMBRE';
			values.verificado = true;

			values.password = crypto.randomBytes(32).toString('hex');

			console.log('--------------VALUES-------------------');
			console.log(values);

			self.create(values, (err, result) => {
				if (err) {
					console.log(err);
				}
				return callback(err, result);
			});
		}
	});
}

usuarioSchema.statics.findOneOrCreateByFacebook = function(profile, callback) {
	const self = this;
	console.log(profile);

	self.findOne({
		$or:[
			{ 'facebookId' : profile.id }, { 'email': profile.emails[0].value }
		]
	}, (err, result) => {
		if(result) {
			callback(err, result);
		} 
		else {
			console.log("--------------CONDITION------------");
			console.log(profile);

			let values = {};

			values.facebookId = profile.id;
			values.email = profile.emails[0].value;
			values.nombre = profile.displayName || 'SIN NOMBRE';
			values.verificado = true;
			values.password = crypto.randomBytes(16).toString('hex');

			console.log('--------------VALUES-------------------');
			console.log(values);

			self.create(values, (err, result) => {
				if (err) {
					console.log(err);
				}
				return callback(err, result);
			});
		}
	});
}

module.exports = mongoose.model('Usuario', usuarioSchema);