const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
	code: Number,
	color: String,
	modelo: String,
	ubicacion: {
		type: [Number], 
		index: {
			type: '2dsphere',
			sparse: true
		}
	}
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
	return new this({
		code: code,
		color: color,
		modelo: modelo,
		ubicacion: ubicacion
	});
}

bicicletaSchema.methods.toString = function() {
	return 'code: ' + this.code + " | color: " + this.color;
}

bicicletaSchema.statics.allBicis = function(cb) {
	this.find({}, cb);
}

bicicletaSchema.statics.add = function(aBici, cb) {
	this.create(aBici, cb);
}

bicicletaSchema.statics.findByCode = function(iCode, cb) {
	this.findOne({code: iCode}, cb);
}

bicicletaSchema.statics.removeByCode = function(iCode, cb) {
	this.deleteOne({code: iCode}, cb);
}

bicicletaSchema.statics.updateByCode = function(iCode, aBici, cb) {
	this.updateOne({code: iCode}, aBici, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

