const mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe("Testing Bicicletas", function(){
	beforeAll(function(done) {
		mongoose.disconnect((err) => {
			if (err) console.log(err);
			var mongoDB = 'mongodb://localhost/testdb';
			mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
			const db = mongoose.connection;
			db.on('error', console.error.bind(console, 'MongoDB Test connection error'));
			db.once('open', function() {
				console.log("We are connected to test database!");
				done();
			});
		});
	});

	afterEach(function(done) {
		Bicicleta.deleteMany({}, function(err, sucess) {
			if (err) console.log(err);
			done();
		});
	});

	describe("Bicicleta.createInstance", () => {
		it('crea una instancia de bicicleta', () => {
			var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
			expect(bici.code).toBe(1);
			expect(bici.color).toBe("verde");
			expect(bici.modelo).toBe("urbana");
			expect(bici.ubicacion[0]).toBe(-34.5);
			expect(bici.ubicacion[1]).toBe(-54.1);
		});
	});

	describe("Bicicleta.allBicis", () => {
		it('comienza vacia', (done) => {
			Bicicleta.allBicis(function(err, bicis) {
				if (err) console.error(err);
				expect(bicis.length).toBe(0);
				done();
			});
		});
	});

	describe("Bicicleta.add", () => {
		it('agrego solo una bici', (done) => {
			var objBici = new Bicicleta({
				code: 1, color: "verde", modelo: "urbana"
			});
			Bicicleta.add(objBici, function(err, nBici) {
				if (err) console.error(err);
				Bicicleta.allBicis(function(err, bicis) {
					if (err) return console.error(err);
					expect(bicis.length).toBe(1);
					expect(bicis[0].code).toBe(objBici.code);
					done();
				});
			});
		});
	});

	describe("Bicicleta.findByCode", () => {
		it('debe devolver la bici con code 1', (done) => {
			Bicicleta.allBicis(function(err, bicis) {
				if (err) console.error(err);
				expect(bicis.length).toBe(0);

				var objBici = new Bicicleta({
					code: 1, color: "verde", modelo: "urbana"
				});

				Bicicleta.add(objBici, function(err, nBici) {
					if (err) console.error(err);
					var objBici2 = new Bicicleta({
						code: 2, color: "roja", modelo: "urbana"
					});
					Bicicleta.add(objBici2, function(err, nBici) {
						if (err) console.error(err);
						Bicicleta.findByCode(1, function(err, targetBici) {
							if (err) return console.error(err);
							expect(targetBici.code).toBe(objBici.code);
							expect(targetBici.color).toBe(objBici.color);
							expect(targetBici.modelo).toBe(objBici.modelo);
							done();
						});
					});
				});
			});
		});
	});

	describe("Bicicleta.removeByCode", () => {
		it('debe eliminar la bici con code 2', (done) => {
			Bicicleta.allBicis(function(err, bicis) {
				if (err) console.error(err);
				expect(bicis.length).toBe(0);

				var objBici = new Bicicleta({
					code: 1, color: "verde", modelo: "urbana"
				});

				Bicicleta.add(objBici, function(err, nBici) {
					if (err) console.error(err);
					var objBici2 = new Bicicleta({
						code: 2, color: "roja", modelo: "urbana"
					});
					Bicicleta.add(objBici2, function(err, nBici) {
						if (err) console.error(err);
						Bicicleta.removeByCode(2, function(err) {
							if (err) return console.error(err);
							Bicicleta.findByCode(2, function(err, targetBici) {
								expect(targetBici).toBe(null);
								done();
							});
						});
					});
				});
			});
		});
	});
});

