var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/usuarios';

describe("Bicicleta API", function(){
	beforeAll(function(done) {
		mongoose.disconnect((err) => {
			if (err) console.log(err);
			var mongoDB = 'mongodb://localhost/testdb';
			mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
			const db = mongoose.connection;
			db.on('error', console.error.bind(console, 'MongoDB Test connection error'));
			db.once('open', function() {
				console.log("We are connected to test database!");
				done();
			});
		});
	});

	afterEach(function(done) {
		Reserva.deleteMany({}, function (err, success) {
			if (err) console.log(err);
			Usuario.deleteMany({}, function (err, success) {
				if (err) console.log(err);
				Bicicleta.deleteMany({}, function (err, success) {
				    if (err) console.log(err);
				    done();
				});
			});
		});
	});

	describe("GET USUARIOS", function(){
		it('Status 200', (done) => {
			request.get(base_url, function(error, response, body) {
				var result = JSON.parse(body);
				expect(response.statusCode).toBe(200);
				expect(result.usuarios.length).toBe(0);
				done();
			});
		});
	});

	describe("GET USUARIOS /:id", function(){
		it('Status 200', (done) => {
			Usuario.find({}, function(err, usuarios) {
				if (err) console.error(err);
				expect(usuarios.length).toBe(0);

				var u = new Usuario({ nombre: 'Manuel' });
				u.save(function(err) {
					if (err) console.error(err);
					request.get(base_url + "/" + u.id, function(error, response, body){
						expect(response.statusCode).toBe(200);
						var user = JSON.parse(body).usuario;
						expect(user.nombre).toBe("Manuel");
						done();
					});
				});
			});
		});
	});

	describe("POST USUARIOS /create", function(){
		it('Status 200', (done) => {
			var headers = {'content-type': 'application/json'};
			var user = '{ "nombre": "Ezequiel" }';

			request.post({
				headers: headers,
				url: base_url + "/create",
				body: user
			}, function(error, response, body) {
				expect(response.statusCode).toBe(200);
				var usuario = JSON.parse(body);
				expect(usuario.nombre).toBe("Ezequiel");
				done();
			});
		});
	});

	describe("POST USUARIOS /reservar", function(){
		it('Status 200', (done) => {
			const usuario = new Usuario({ nombre: 'Jose' });
			usuario.save();
			const bicicleta = new Bicicleta({ code: 1, color: 'azul', modelo: 'urbana' });
			bicicleta.save();

			var headers = {'content-type': 'application/json'};
			var reserva = `{ "id": "${usuario.id}", "bici_id": "${bicicleta.id}", "desde": "2020-10-31T03:07:54.640Z", "hasta": "2020-11-01T03:07:54.640Z" }`;
			console.log(reserva);

			request.post({
				headers: headers,
				url: base_url + "/reservar",
				body: reserva
			}, function(error, response, body){
				expect(response.statusCode).toBe(200);
				var newReserva = JSON.parse(body).reserva;
				expect(newReserva.desde).toBe("2020-10-31T03:07:54.640Z");
				expect(newReserva.hasta).toBe("2020-11-01T03:07:54.640Z");
				expect(newReserva.bicicleta).toBe(bicicleta.id);
				expect(newReserva.usuario).toBe(usuario.id);
				done();
			});
		});
	});
});