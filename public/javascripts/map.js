var map = L.map('main_map').setView([-4.002036, -79.2083404], 16);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([-4.001483, -79.205960]).addTo(map);
// L.marker([-4.001725, -79.209431]).addTo(map);
// L.marker([-3.999663, -79.207479]).addTo(map);

$.ajax({
	url: '/api/bicicletas',
	type: 'GET',
	dataType: 'json',
	success: function(result) {
		console.log(result);
		result.bicicletas.forEach(
			(bici) => L.marker(bici.ubicacion, {title: bici.code}).addTo(map)
		);
	}
});
