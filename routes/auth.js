var express = require('express');
var router = express.Router();
var ctAuth = require('../controllers/ctAuth');

router.get('/login', ctAuth.auth_login_get);
router.post('/login', ctAuth.auth_login_post);
router.get('/logout', ctAuth.auth_logout);
router.get('/forgotPassword', ctAuth.auth_forgot_password_get);
router.post('/forgotPassword', ctAuth.auth_forgot_password_post);
router.get('/resetPassword/:token', ctAuth.auth_reset_password_get);
router.post('/resetPassword', ctAuth.auth_reset_password_post);

// OAuth Google
router.get('/google', ctAuth.auth_google);
router.get('/google/callback', ctAuth.auth_google_callback);

// OAuth Facebook
router.get('/facebook', ctAuth.auth_facebook);
router.get('/facebook/callback', ctAuth.auth_facebook_callback);

module.exports = router;