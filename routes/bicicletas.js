var express = require('express');
var router = express.Router();
var ctBicicleta = require('../controllers/ctBicicleta');

router.get('/', ctBicicleta.bicicleta_list);
router.get('/create', ctBicicleta.bicicleta_create_get);
router.post('/create', ctBicicleta.bicicleta_create_post);
router.get('/delete/:code', ctBicicleta.bicicleta_delete_get);
router.get('/update/:code', ctBicicleta.bicicleta_update_get);
router.post('/update/:code', ctBicicleta.bicicleta_update_post);
router.get('/watch/:code', ctBicicleta.bicicleta_watch_get);

module.exports = router;