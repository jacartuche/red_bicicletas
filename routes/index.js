var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Red Bicicletas', user: req.user });
});

/* GET Policy privacy */
router.get('/privacy_policy', function(req, res, next) {
  res.sendfile('public/policy_privacy.html');
});

/* GET Terms and Conditions of Use */
router.get('/terms_conditions_service', function(req, res, next) {
  res.sendfile('public/terms_conditions_service.html');
});

module.exports = router;
