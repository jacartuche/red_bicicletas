var express = require('express');
var router = express.Router();
var ctToken = require('../controllers/ctToken');

router.get('/confirmation/:token', ctToken.confirmation_get);

module.exports = router;