var express = require('express');
var router = express.Router();
var ctUsuarios = require('../controllers/ctUsuarios');

router.get('/', ctUsuarios.usuario_list);
router.get('/create', ctUsuarios.usuario_create_get);
router.post('/create', ctUsuarios.usuario_create_post);
router.get('/update/:id', ctUsuarios.usuario_update_get);
router.post('/update/:id', ctUsuarios.usuario_update_post);
router.post('/delete/:id', ctUsuarios.usuario_delete_post);

module.exports = router;